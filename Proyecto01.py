#Nombre: Gabriela Antonia Sepúlveda Rojas
#RUT: 20.787.581-3
#Fecha de entrega: 12 de octubre del 2021

#Se requiere crear un simulador de secuencias (pasos) dado ciertos parámetros y una dimensi´on de N x N (matriz). La idea 
#es crear una matriz de una dimensión aleatoria y dentro de la estructura conseguir que elementos logren interactuar de 
#modo que podamos simular un comportamiento dado situaciones controladas.

#Entrada: Se le pedirá al usuario que ingrese un número cualquiera, el cual será considerado para la creación de una matriz
#N x N, de tal forma que será proporcional.

#Desarrollo: Se deberá ingresar al sistema 5 letras con diferentes condiciones: 
# Letra A: Se mantendrá inmovil y hará rebotar a la partícula.
# Letra E: Cuando sea alcanzado por la partícula regresará al cuadrante inicial o final.
# Letra F: Si la partícula le alcanza dará por finalizado el mundo.
# Letra S: Letra de superpoder que nos ayudará a eliminar todas las letras que alcancen un lapsus de 10 pasos.

# Letra B: e cada vez que se impacte se mover´a un espacio de acuerdo a la dirección del impacto. Esto sería, si la 
# letra B está en el cuadrante 4 x 4 y la “partícula” la impacta desde arriba, la letra B se moverá a la posición 5 x 4 
# y la “partícula” rebotará en algún sentido para dar continuidad a la ejecución. Si el impacto es efectivo desde la 
# izquierda en dirección a la derecha, entonces se moverá al cuadrante 4 x 5.


#Salida: Se mostrará en la pantalla una matriz dependiendo el tamaño que haya elegido el usuario, con 5 letras diferentes,
#a las cuales ya les hemos ingresado instrucciones en caso de que la partícula las toque. De forma aleatoría la partícula
#se moverá hasta que se de por terminado el mundo.

import random

dimension = int(input("Ingresar el tamaño (dimensión) de la matriz: "))
matriz = []

for x in range(0, dimension):
    matriz.append([' '] * dimension)

numRows = random.randint(0, dimension - 1)  #Posicion de fila para partícula
numCols = random.randint(0, dimension - 1)  #Posicion de columna para partícula
numRowsA = random.randint(0, dimension - 1) #Posicion de fila para la letra A
numColsA = random.randint(0, dimension - 1) #Posicion de columna para la letra A
#Posiciones = [[1,2],[1,2]]
contador = 0

#flag_1 = True

while True:
# Neceitamos guardar la posicion anterior de la matriz la saber de que posicion viene la particula. Esto es para la letra B
    if contador == 0:
        fila = numRows
        columna = numCols
    if contador == 1:
        contador = -1

    contador += 1

    numRowsE = random.randint(0, dimension - 1)  #Posición de fila para la letra E
    numColsE = random.randint(0, dimension - 1)  #Posición de columna para la letra E

    numRowsF = random.randint(0, dimension - 1)  #Posición de fila para la letra F
    numColsF = random.randint(0, dimension - 1)  #Posición de columna para la letra F

    numRowsS = random.randint(0, dimension - 1)  #Posición de fila para la letra S
    numColsS = random.randint(0, dimension - 1)  #Posición de columna para la letra S

    numRowsB = random.randint(0, dimension - 1)  #Posición de fila para la letra B
    numColsB = random.randint(0, dimension - 1)  #Posición de columna para la letra B

    #if flag_1:
    matriz[numRows][numCols] = '*'

    #matriz[1][1] = "A"

    if "*" in matriz[numRowsA][numColsA]:
        matriz[numRowsA][numColsA] += "A"

    elif matriz[numRowsA][numColsA] == " ":
        matriz[numRowsA][numColsA] = "A"

    else:
        matriz[numRowsA][numColsA] += "A"


    if "*" in matriz[numRowsE][numColsE]:
        matriz[0][0] = "E"

        #matriz[numRowsE][numColsE] += "E"

    elif matriz[numRowsE][numColsE] == " ":
        matriz[numRowsE][numColsE] = "E"

    else:
        matriz[numRowsE][numColsE] += "E"


    if "*" in matriz[numRowsF][numColsF]:
        matriz[numRowsF][numColsF] += "F"
        print("Fin del mundo")
        break

    elif matriz[numRowsF][numColsF] == " ":
        matriz[numRowsF][numColsF] = "F"

    else:
        matriz[numRowsF][numColsF] += "F"


    if "*" in matriz[numRowsB][numColsB]:
        matriz[numRowsB][numColsB] += "B"
        print("VALIDANDO!")

        #Validamos si la particula lo choca por arriba.

        if columna == numColsB and fila+1 == numRowsB:
            print("Lo chocó por arriba")

            if numRowsF+1 < len(matriz[0]):

                if matriz[numRowsB+1][numColsB] == " ":
                    matriz[numRowsB+1][numColsB] = "B"

                else:
                    matriz[numRowsB+1][numColsB] += "B"

        #Validamos si la particula lo choca por la izquierda.

        elif fila == numRows and numColsB == columna+1:
            print("Lo chocó por la izquierda")

            if numColsB+1 < len(matriz[0]):

                if matriz[numRowsB][numColsB+1] == " ":
                    matriz[numRowsB][numColsB+1] = "B"

                else:
                    matriz[numRowsB][numColsB+1] += "B"

        #Validamos si la particula lo choca por derecha.

        elif fila == numRows and numColsB == columna-1:
            print("Lo chocó por la derecha")

            if numColsB-1 >= 0:

                if matriz[numRowsB][numColsB-1] == " ":
                    matriz[numRowsB][numColsB-1] = "B"

                else:
                    matriz[numRowsB][numColsB-1] += "B"


        #Validamos si la particula lo choca por abajo.

        elif columna == numColsB and fila-1 == numRowsB:
            print("Lo chocó por abajo")

            if numRowsF-1 >= 0:

                if matriz[numRowsB - 1][numColsB] == " ":
                    matriz[numRowsB - 1][numColsB] = "B"

                else:
                    matriz[numRowsB-1][numColsB] += "B"


    elif matriz[numRowsB][numColsB] == " ":
        matriz[numRowsB][numColsB] = "B"

    else:
        matriz[numRowsB][numColsB] += "B"


    if "*" in matriz[numRowsS][numColsS]:
        salir = 0
        print("La particula a alcanzado la letra S y la partícula eliminará las letras que encuentre en 10 pasos")

        while salir < 10:
            numRows = random.randint(0, dimension - 1)  #Posición de fila para eliminar la letra en un lapsus de 10 pasos
            numCols = random.randint(0, dimension - 1)  #Posición de columna para eliminar la letra en un lapsus de 10 pasos
            matriz[numRows][numCols] = ' '
            salir += 1;
        matriz[numRows][numCols] = '*'

    elif matriz[numRowsS][numColsS] == " ":
        matriz[numRowsS][numColsS] = "S"

    else:
        matriz[numRowsS][numColsS] += "S"

    print("   ", end='')

    for numFila in range(0, dimension):
        print("{0:>3}".format(numFila + 1), end='')
    print()

    for rows in range(0, dimension):
        print("{0:>2}".format(rows + 1), end=' ')

        for cols in range(0, dimension):
            print("| " + matriz[rows][cols], end='')
        print("|")

    for rows in range(0, dimension):

        for cols in range(0, dimension):
            matriz[rows][cols] = " "

    matriz[numRows][numCols] = ' '

    #Movimiento de la partícula

    if dimension > 1:
        aux_filas = numRows
        aux_cols = numCols

        while aux_filas == numRows and aux_cols == numCols:
            datax = random.randint(1, 4)

            if datax == 1:

                if numRows >= 0 and numRows < (dimension - 1):
                    numRows += 1

            if datax == 2:

                if numCols >= 0 and numCols < (dimension - 1):
                    numCols += 1

            if datax == 3:

                if numCols > 0 and numCols <= (dimension - 1):
                    numCols -= 1

            if datax == 4:

                if numRows > 0 and numRows <= (dimension - 1):
                    numRows -= 1
    else:

        break

    input("\nPresionar enter para mover la partícula\n")